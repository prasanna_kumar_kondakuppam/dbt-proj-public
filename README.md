# Dbt Proj



## Procedure



##Ingestion of data into snowflake

Activate .snow python environment
source .snow/bin/activate
copy first version of files to be processed in the process directory
DE_casestudy_charges_1.csv
DE_casestudy_datapoints_1.csv
DE_casestudy_ports.csv
DE_casestudy_regions.csv
DE_casestudy_exchange_rates.csv


Create necessary artifacts in snowflake

create database if not exists dataprocessing;
use database dataprocessing;
create schema raw;


Run the python program
Python snowfiles_movement.py

(The program creates all the required snowflake table and stages and load the data into the raw schema )


## Transformation using dbt
use python virtual env dbt-env
make necessary profile file connecting to snowflake connection 
Once connection is successful
do run dbt build --full-refresh (runs all models,tests,sources,snapshots,incremental models)


##Repeat the step with loading the remaining files in snowflake by following the same pattern as given in Ingestion of data section

## Run Transformation pipeline with dbt build command

Observed all the data is present in schemas raw,staging,final 

01:base -- is basically reading for source as views in raw layer.

This can be used for column naming conventions or other sorts of standardization.
Components sits as views in raw schema

02-conformance:

This is standardization where the incremental are places where ever necessary.
The main aim of this layer is to materialize as tables since compute is costlier and storage is way cheaper now

It helps to improve perfomance

components sits as tables in staging layer

03-Integration
This is the logic to ingregate all another sources and bring into a shape for agreegations.
This can be either table or view depending on usecase
Here the components sit in staging schema as view

04-Business:

Agregations required for business is placed here.
All other aggregations required by business can be designed in similar fashion


snapshots:

Snapshots is designed for currency to always deal with right and latest currency value if there is frequent updates









