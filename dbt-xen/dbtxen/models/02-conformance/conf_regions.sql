
WITH RECURSIVE region_hierarchy AS (
    SELECT
        {{ get_column_names('base_raw_regions',[],'') }}
        ,SLUG AS root_slug
        ,NAME AS root_name
        ,0 AS level
    FROM  {{ ref("base_raw_regions") }}
    WHERE PARENT IS NULL

    UNION ALL

    SELECT
        {{ get_column_names('base_raw_regions',[],'child') }}
        ,parent.root_slug
        ,parent.root_name
        ,parent.level + 1 AS level
    FROM  {{ ref("base_raw_regions") }} child
    JOIN region_hierarchy parent ON child.PARENT = parent.SLUG
)
 

select 
*
,'{{ invocation_id }}' as job_run_id
, current_timestamp()  as loaded_timestamp
 from region_hierarchy

