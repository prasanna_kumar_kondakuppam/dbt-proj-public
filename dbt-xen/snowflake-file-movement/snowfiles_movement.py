import snowflake.connector
import os
import shutil
import snowflake_creds

# Snowflake Configuration
SNOWFLAKE_USER = snowflake_creds.SNOWFLAKE_USER
SNOWFLAKE_PASSWORD = snowflake_creds.SNOWFLAKE_PASSWORD
SNOWFLAKE_ACCOUNT = snowflake_creds.SNOWFLAKE_ACCOUNT
SNOWFLAKE_WAREHOUSE = 'compute_wh'
SNOWFLAKE_DATABASE = 'dataprocessing'
SNOWFLAKE_SCHEMA = 'raw'
#SNOWFLAKE_TABLE_NAME = 'datapoints'
LOCAL_FOLDER_PATH = './process'
DESTINATION_FOLDER_PATH = './processed'

# Connect to Snowflake
def connect_to_snowflake():
    try:
        ctx = snowflake.connector.connect(
            user=SNOWFLAKE_USER,
            password=SNOWFLAKE_PASSWORD,
            account=SNOWFLAKE_ACCOUNT,
            warehouse=SNOWFLAKE_WAREHOUSE,
            database=SNOWFLAKE_DATABASE,
            schema=SNOWFLAKE_SCHEMA
        )
        print("Connected to Snowflake")
        return ctx
    except snowflake.connector.Error as e:
        print(f"Error connecting to Snowflake: {e}")
        return None

# Function to read SQL file
def read_sql_file(filename):
    with open(filename, 'r') as file:
        return file.read()

# Function to create a Snowflake stage
def create_snowflake_stage(ctx, stage_name):
    try:
        cs = ctx.cursor()
        create_stage_sql = f"CREATE STAGE IF NOT EXISTS  {stage_name}"
        cs.execute(create_stage_sql)
        print(f"Created Snowflake stage: {stage_name}")
        cs.close()
    except snowflake.connector.Error as e:
        print(f"Error creating Snowflake stage: {e}")

# Function to create a Snowflake table
def create_snowflake_table(ctx,sqlfile):
    try:
        cs = ctx.cursor()
        create_data_table = read_sql_file(sqlfile)
        cs.execute(create_data_table)
        print(f"Executed:  {sqlfile}")
        cs.close()
    except snowflake.connector.Error as e:
        print(f"Error creating Snowflake table: {e}")


# Function to upload CSV files to Snowflake
def load_data_from_snowflake_stage(cs, local_csv_path, stage_name,SNOWFLAKE_TABLE_NAME,pattern):
    try:
        if  SNOWFLAKE_TABLE_NAME:
            print(SNOWFLAKE_TABLE_NAME)       
            copy_into_sql = f"""
            COPY INTO {SNOWFLAKE_TABLE_NAME}
            FROM @{stage_name}
            FILE_FORMAT = (TYPE = 'CSV' FIELD_OPTIONALLY_ENCLOSED_BY='"' SKIP_HEADER = 1)
            pattern='{pattern}';
            """
            print(copy_into_sql)
            cs.execute(copy_into_sql)
            print(f"Loaded data into {SNOWFLAKE_TABLE_NAME} from {local_csv_path}")
            shutil.move(local_csv_path, os.path.join(DESTINATION_FOLDER_PATH, os.path.basename(local_csv_path)))
            print(f"Moved {local_csv_path} to {DESTINATION_FOLDER_PATH}")
    except snowflake.connector.Error as e:
        print(f"Error uploading CSV to Snowflake: {e}")

# Function to upload CSV files to Snowflake
def upload_csv_to_snowflake(ctx, local_csv_path, stage_name):
    try:
        cs = ctx.cursor()
        put_file_sql = f"PUT file://{local_csv_path} @{stage_name} AUTO_COMPRESS=FALSE"
        put_result = cs.execute(put_file_sql).fetchone()
        print(f"Uploaded {local_csv_path} to Snowflake stage {stage_name}")
        print(os.path.basename(local_csv_path))
        print(put_result)
        if put_result[6] == 'UPLOADED':
            if os.path.basename(local_csv_path).startswith('DE_casestudy_datapoints'):
                pattern='.*DE_casestudy_datapoints.*'
                SNOWFLAKE_TABLE_NAME='DATAPROCESSING.RAW.DATAPOINTS'
                load_data_from_snowflake_stage(cs, local_csv_path, stage_name,SNOWFLAKE_TABLE_NAME,pattern)
            elif os.path.basename(local_csv_path).startswith('DE_casestudy_charges'):
                pattern='.*DE_casestudy_charges.*'
                SNOWFLAKE_TABLE_NAME='DATAPROCESSING.RAW.CHARGES'
                load_data_from_snowflake_stage(cs, local_csv_path, stage_name,SNOWFLAKE_TABLE_NAME,pattern)
            elif os.path.basename(local_csv_path).startswith('DE_casestudy_ports'):
                pattern='.*DE_casestudy_ports.*'
                SNOWFLAKE_TABLE_NAME='DATAPROCESSING.RAW.PORTS'
                load_data_from_snowflake_stage(cs, local_csv_path, stage_name,SNOWFLAKE_TABLE_NAME,pattern)
            elif os.path.basename(local_csv_path).startswith('DE_casestudy_exchange_rates'):
                pattern='.*DE_casestudy_exchange_rates.*'
                SNOWFLAKE_TABLE_NAME='DATAPROCESSING.RAW.EXCHANGE_RATES'
                load_data_from_snowflake_stage(cs, local_csv_path, stage_name,SNOWFLAKE_TABLE_NAME,pattern)
            elif os.path.basename(local_csv_path).startswith('DE_casestudy_regions'):
                pattern='.*DE_casestudy_regions.*'
                SNOWFLAKE_TABLE_NAME='DATAPROCESSING.RAW.REGIONS'
                load_data_from_snowflake_stage(cs, local_csv_path, stage_name,SNOWFLAKE_TABLE_NAME,pattern)
        cs.close()
    except snowflake.connector.Error as e:
        print(f"Error uploading CSV to Snowflake: {e}")


      

# Main function to orchestrate the process
def main():
    ctx = connect_to_snowflake()
    create_data_points_sql='create_datapoints.sql'
    create_charges_sql='create_charges.sql'
    create_exchange_rates_sql='create_exchangerates.sql'
    create_ports_sql='create_ports.sql'
    create_regions_sql='create_regions.sql'
    if ctx:
        try:
            create_snowflake_stage(ctx, 'temp_stage')
            #create tables
            create_snowflake_table(ctx,create_data_points_sql)
            create_snowflake_table(ctx,create_charges_sql)
            create_snowflake_table(ctx,create_exchange_rates_sql)
            create_snowflake_table(ctx,create_ports_sql)
            create_snowflake_table(ctx,create_regions_sql)

            for filename in os.listdir(LOCAL_FOLDER_PATH):
                if filename.endswith('.csv'):
                    local_csv_path = os.path.join(LOCAL_FOLDER_PATH, filename)
                    upload_csv_to_snowflake(ctx, local_csv_path, 'temp_stage')

        finally:
            ctx.close()

# Execute the main function
if __name__ == "__main__":
    main()
