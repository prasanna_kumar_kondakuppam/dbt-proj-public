with source as (
      select * from {{ source('raw', 'regions') }}
),
renamed as (
    select      
    *
    from source
)
select * from renamed
  