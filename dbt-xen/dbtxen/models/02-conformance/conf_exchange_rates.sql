
WITH conf_currency AS (
    SELECT
        *
        
    FROM  {{ ref("snap_currency") }}  where dbt_valid_to is null
)
 

select 
*
--,'{{ invocation_id }}' as job_run_id
, current_timestamp()  as loaded_timestamp
 from conf_currency

