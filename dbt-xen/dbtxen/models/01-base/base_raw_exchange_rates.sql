with source as (
      select * from {{ source('raw', 'exchange_rates') }}
),
renamed as (
    select
        *
    from source
)
select * from renamed
  