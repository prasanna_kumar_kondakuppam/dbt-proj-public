-- CTE to get the necessary columns and perform aggregations
with int_source as (
    select 
        {{ get_column_names('int_all_sources', ['USD_CONVERSION_VALUE', 'CHARGE_VALUE','JOB_RUN_ID','LOADED_TIMESTAMP'], '') }}
        ,sum(usd_conversion_value) as total_value_of_usd
    from {{ ref("int_all_sources") }}
    group by 
    {{ get_column_names('int_all_sources', ['USD_CONVERSION_VALUE', 'CHARGE_VALUE','JOB_RUN_ID','LOADED_TIMESTAMP'], '') }}
),

-- CTE to perform data quality checks
dq_check as (
    select 
        equipment_id,
        originating_region,
        desination_region,
        count(distinct company_id) as comp_count,
        count(distinct supplier_id) as supplier_count
    from {{ ref("int_all_sources") }}
    group by equipment_id, originating_region, desination_region
    having count(distinct company_id) >= 5 and count(distinct supplier_id) >= 2
),

-- Final selection and join to add dq_status column
final_selection as (
    select 
        a.*,
        case 
            when b.equipment_id is not null 
                and b.originating_region is not null 
                and b.desination_region is not null 
            then true 
            else false 
        end as dq_ok
    from int_source a
    left join dq_check b
    on a.equipment_id = b.equipment_id
    and a.originating_region = b.originating_region
    and a.desination_region = b.desination_region
)

-- Select from final_selection CTE
select * from final_selection
