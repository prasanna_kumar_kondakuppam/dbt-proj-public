{{
    config(
        materialized='incremental',
        on_schema_change='fail'
    )
}}


with src_conf_charges as (
select {{ get_column_names('base_raw_charges',[],'') }}

from 
 {{ ref("base_raw_charges") }}
{% if is_incremental() %}
  where (d_id,currency,charge_value) not in ( select d_id,currency,charge_value from {{ this }} )
{% endif %} 
)

select *
,'{{ invocation_id }}' as job_run_id
, current_timestamp()  as loaded_timestamp 
 from src_conf_charges
  