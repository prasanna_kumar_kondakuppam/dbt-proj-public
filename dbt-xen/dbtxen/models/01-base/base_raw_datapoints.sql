with source as (
      select * from {{ source('raw', 'datapoints') }}
),
renamed as (
    select
        *
    from source
)
select * from renamed
  