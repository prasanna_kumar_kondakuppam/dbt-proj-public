{{
    config(
        materialized='incremental',
        on_schema_change='fail'
    )
}}


with src_raw_datapoints as (
select * from {{ ref("base_raw_datapoints") }}

{% if is_incremental() %}
  where d_id not in ( select d_id from {{ this }} )
{% endif %} 
)
select 
*
,'{{ invocation_id }}' as job_run_id
, current_timestamp()  as loaded_timestamp
 from src_raw_datapoints
  