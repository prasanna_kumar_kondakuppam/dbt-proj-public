create table if not exists DATAPROCESSING.RAW.EXCHANGE_RATES (
	DAY DATE,
	CURRENCY VARCHAR(16777216),
	RATE VARCHAR(16777216)
);