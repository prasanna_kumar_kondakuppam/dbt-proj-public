{% macro get_column_names(model, exceptioncolumns=[], alias='') -%}
    {%- set columns = [] -%}
    {%- for col in adapter.get_columns_in_relation(ref(model)) -%}
        {%- if col.name not in exceptioncolumns -%}
            {%- if alias == '' -%}
                {%- set _ = columns.append(col.name | lower) -%}
            {%- else -%}
                {%- set _ = columns.append(alias ~ '.' ~ (col.name | lower)) -%}
            {%- endif -%}
        {%- endif -%}
    {%- endfor -%}
    {{ columns | join(', ') }}
{%- endmacro %}

{{ get_column_names('int_all_sources',['USD_CONVERSION_VALUE','CHARGE_VALUE'],'') }}
