{% snapshot snap_currency %}

{{
    config(
      target_schema='staging',
      unique_key='curr_id',
      strategy='check',
      check_cols=['day', 'currency','rate']
    )
}}

select 
    {{ dbt_utils.generate_surrogate_key(['day', 'currency','rate']) }} as curr_id
    ,day
    ,currency
    ,rate
    ,'{{ invocation_id }}' as job_run_id
 from {{ref('base_raw_exchange_rates')}}

{% endsnapshot %}