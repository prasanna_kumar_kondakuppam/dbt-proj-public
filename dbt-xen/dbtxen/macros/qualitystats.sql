{% macro qualitystats(database,schema) %}
    {% set create_table_sql %}
        create table if not exists {{ database }}.{{ schema }}.stats_table (
            originating_country varchar,
            destination_country varchar,
            equipment_id varchar,
            created date,
            dq_ok boolean,
            JOB_RUN_ID varchar,
            LOADED_TIMESTAMP timestamp
            
        );
    {% endset %}

    {% set insert_data_sql %}
        insert into {{ database }}.{{ schema }}.stats_table
        select distinct 
            oringating_country, 
            destination_country, 
            equipment_id, 
            cast(created as date) as created, 
            dq_ok 
            ,'{{ invocation_id }}' as job_run_id
            , current_timestamp()  as loaded_timestamp
        from {{ this }};
    {% endset %}

    {% do run_query(create_table_sql) %}
    {% do run_query(insert_data_sql) %}
{% endmacro %}
