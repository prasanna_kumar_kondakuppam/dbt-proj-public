
WITH conf_ports AS (
    SELECT
        {{ get_column_names('base_raw_ports',[],'') }}
        
    FROM  {{ ref("base_raw_ports") }} 
)
 

select 
*
,'{{ invocation_id }}' as job_run_id
, current_timestamp()  as loaded_timestamp
 from conf_ports

