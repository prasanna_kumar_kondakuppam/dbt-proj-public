with source as (
      select * from {{ source('raw', 'ports') }}
),
renamed as (
    select p.*
    from source p
    qualify row_number() over (partition by pid order by pid asc )=1
) 

select * from renamed
  