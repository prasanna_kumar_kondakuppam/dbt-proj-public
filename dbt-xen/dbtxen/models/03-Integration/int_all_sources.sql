
WITH conf_charges AS (
    SELECT
        {{ get_column_names('conf_charges',['JOB_RUN_ID','LOADED_TIMESTAMP'],'') }}
        
    FROM  {{ ref("conf_charges") }} 
)

, conf_datapoints AS (
    SELECT
        {{ get_column_names('conf_datapoints',['JOB_RUN_ID','LOADED_TIMESTAMP'],'') }}
        
    FROM  {{ ref("conf_datapoints") }} 
)

,conf_exchange_rates AS (
    SELECT
        {{ get_column_names('conf_exchange_rates',['JOB_RUN_ID','LOADED_TIMESTAMP'],'') }}
        
    FROM  {{ ref("conf_exchange_rates") }} 
)
, conf_ports AS (
    SELECT
        {{ get_column_names('conf_ports',['JOB_RUN_ID','LOADED_TIMESTAMP'],'') }}
        
    FROM  {{ ref("conf_ports") }} 
)
,conf_regions AS (
    SELECT
        {{ get_column_names('conf_regions',['JOB_RUN_ID','LOADED_TIMESTAMP'],'') }}
        
    FROM  {{ ref("conf_regions") }} 
)
 
 



select 

dp.*
,ch.currency
,ch.charge_value
,curr.currency as curreny_mapp
,curr.rate as exchange_rate
,poo.code as orinating_port_code
,pod.code as destination_code
,poo.slug as originating_slug
,pod.slug as destination_slug
,poo.name as oringating_port_name
,pod.name as destination_port_name
,poo.country as oringating_country
,pod.country as destination_country
,orregion.root_name as originating_region
,deregion.root_name as desination_region
,charge_value * exchange_rate as usd_conversion_value
,'{{ invocation_id }}' as job_run_id
, current_timestamp()  as loaded_timestamp
from 
conf_datapoints dp

left join
conf_charges ch
on dp.D_ID=ch.D_ID

left join
conf_exchange_rates curr
on curr.currency= ch.currency and curr.day =cast(dp.created as date)

left join 
conf_ports poo
on poo.pid=dp.origin_pid

left join 
conf_ports pod
on pod.pid=dp.destination_pid

left join
conf_regions orregion
on orregion.slug = poo.slug 

left join
conf_regions deregion
on deregion.slug = pod.slug 